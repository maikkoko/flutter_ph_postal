import 'package:flutter/material.dart';
import 'cities_tab.dart';
import 'provinces_tab.dart';
import 'postal_codes_tab.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<Widget> _pages = [
    ProvincesTab(),
    CitiesTab(),
    PostalCodesTab(),
  ];

  int _currentIndex = 1;

  _onTabChange(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Home'),
      ),
      body: _pages[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _onTabChange,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.looks_one),
            title: new Text('Province'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.looks_two),
            title: new Text('City'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.looks_3),
            title: Text('Postal Codes')
          )
        ],
      ),
    );
  }
}