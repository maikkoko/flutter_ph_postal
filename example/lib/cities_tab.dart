import 'dart:async';
import 'package:flutter/material.dart';
import 'package:ph_postal_codes/database/database_client.dart';
import 'package:ph_postal_codes/models/city.dart';

class CitiesTab extends StatelessWidget {
  final searchController = TextEditingController();

  Future _onSearch() async {
    final String searchInput = searchController.value.text;

    final dbClient = new DatabaseClient();
    await dbClient.open();
    
    final provider = new CityProvider(dbClient.db);
    List<City> results = await provider.searchByName(searchInput);

    results.forEach((city) {
      print(city.name);
    });

    await dbClient.close();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 8.0),
      child: new Column(
        children: <Widget>[
          TextFormField(
            controller: searchController,
            decoration: InputDecoration(
              labelText: 'Search City'
            ),
          ),
          new SizedBox(
            width: double.infinity,
            child: new RaisedButton(
              onPressed: _onSearch,
              color: Colors.blue,
              child: new Text('Search',
                style: new TextStyle(
                  color: Colors.white,
                ),
              )
            ),
          )
        ]
      )
    );
  }
}