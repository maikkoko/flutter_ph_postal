import 'package:flutter/material.dart';
import 'package:ph_postal_codes/ph_postal_codes.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

    _initializeData();
  }

  _initializeData() {
    final postal = new PHPostalCodes();

    postal.init()
      .then((_) {
        _pushToHome();
      });
  }

  _pushToHome() {
    Navigator.of(context).pushReplacementNamed('/home');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: new CircularProgressIndicator(),
        ),
      )
    );
  }
}