import 'package:flutter/material.dart';
import 'home.dart';
import 'splash.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'PH Postal Example',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new SplashScreen(),
      routes: {
        '/home': (context) => new Home(),
      },
    );
  }
}
