library ph_postal_codes;

import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'models/models.dart';
import 'database/database_client.dart';

enum Data {
  cities,
  provinces,
  postal_codes,
}

const String cityAssetsLocation = 'assets/cities.json';
const String provincesAssetsLocation = 'assets/provinces.json';
const String postalCodesAssetsLocation = 'assets/postal_codes.json';

const TAG = '[PostalPH]';

class PHPostalCodes {
  init() async {  
    final dbClient = new DatabaseClient();
    await dbClient.open();

    bool isInit = await this._isInit(dbClient);

    await dbClient.close();

    if (!isInit) {
      _writeData();
    }
  }

  // Returns true if data is stored in database
  _isInit(DatabaseClient dbClient) async {
    final provinces = await _getAllProvinces(dbClient);
    final cities = await _getAllCities(dbClient);
    final postalAreas = await _getAllPostalAreas(dbClient);

    return (provinces.length > 0 && cities.length > 0 && postalAreas.length > 0);
  }

  _writeData() async {
    final dbClient = new DatabaseClient();
    await dbClient.delete();
    await dbClient.open();

    print('$TAG Inserting new data...');

    await _writeProvinces(dbClient);
    await _writeCities(dbClient);
    await _writePostalAreas(dbClient);
    await _isInit(dbClient);

    await dbClient.close();
  }

  Future _writeProvinces(DatabaseClient dbClient) async {
    String provincesMap = await _loadAsset(Data.provinces);
    final parsed = json.decode(provincesMap).cast<Map<String, dynamic>>();
    List<Province> provinces = parsed.map<Province>((map) => Province.fromMap(map)).toList();

    final provinceProvider = new ProvinceProvider(dbClient.db);
    await provinceProvider.batchInsert(provinces);
  }

  Future<List<Province>> _getAllProvinces(DatabaseClient dbClient) async {
    final provinceProvider = new ProvinceProvider(dbClient.db);
    List<Province> provinces = await provinceProvider.getAll();
    print('$TAG Found ${provinces.length} provinces.');

    return provinces;
  }

  Future _writeCities(DatabaseClient dbClient) async {
    String citiesMap = await _loadAsset(Data.cities);
    final parsed = json.decode(citiesMap).cast<Map<String, dynamic>>();
    List<City> cities = parsed.map<City>((cityJson) => City.fromMap(cityJson)).toList();

    final cityProvider = new CityProvider(dbClient.db);
    await cityProvider.batchInsert(cities);
  }

  Future<List<City>> _getAllCities(DatabaseClient dbClient) async {
    final cityProvider = new CityProvider(dbClient.db);
    final cities = await cityProvider.getAll();
    print('$TAG Found ${cities.length} cities.');

    return cities;
  }

  Future _writePostalAreas(DatabaseClient dbClient) async {
    String postalMap = await _loadAsset(Data.postal_codes);
    final parsed = json.decode(postalMap).cast<Map<String, dynamic>>();

    List<PostalAreaList> postalData = parsed.map<PostalAreaList>((map) => PostalAreaList.fromMap(map)).toList();
    List<PostalArea> postalAreas = <PostalArea>[];
    postalData.forEach((data) {
      postalAreas.addAll(PostalArea.fromList(data));
    });  

    final postalProvider = new PostalAreaProvider(dbClient.db);
    await postalProvider.batchInsert(postalAreas);
  }

  Future<List<PostalArea>> _getAllPostalAreas(DatabaseClient dbClient) async {
    final postalProvider = new PostalAreaProvider(dbClient.db);
    final provinces = await postalProvider.getAll();
    print('$TAG Found ${provinces.length} postal data.');

    return provinces; 
  }

  Future<String> _loadAsset(Data asset) async {
    String assetLocation;
    switch(asset) {
      case Data.cities:
        assetLocation = cityAssetsLocation;
        break;

      case Data.provinces:
        assetLocation = provincesAssetsLocation;
        break;

      case Data.postal_codes:
        assetLocation = postalCodesAssetsLocation;
        break;

      default: break;
    } 

    return await rootBundle.loadString(assetLocation);
  }
}