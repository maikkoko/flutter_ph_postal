import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:meta/meta.dart';

class PostalAreaList {
  final int provinceId;
  final List<dynamic> data;

  PostalAreaList({
    @required this.provinceId,
    @required this.data,
  });

  factory PostalAreaList.fromMap(Map<String, dynamic> map) {
    return PostalAreaList(
      provinceId: map['province_id'] as int,
      data: map['data'],
    );
  }
}


class PostalArea {
  static const String tableName = 'postal_areas';
  static const String columnId = 'id';
  static const String columnLocation = 'location';
  static const String columnCityId = 'citymun_id';
  static const String columnProvinceId = 'province_id';
  static const String columnPostalCode = 'zip_code';
  static const String columnPhoneAreaCode = 'phone_area_code';

  final int id;
  final int cityId;
  final int provinceId;
  final String location;
  final String postalCode;
  final String phoneAreaCode;

  PostalArea({
    this.id,
    @required this.location,
    @required this.cityId,
    @required this.provinceId,
    @required this.postalCode,
    @required this.phoneAreaCode,
  });

  factory PostalArea.fromMap(Map<String, dynamic> map, [int provinceId]) {
    return PostalArea(
      id: map[columnId] != null ? map[columnId] : null,
      location: map[columnLocation],
      cityId: map[columnCityId],
      provinceId: provinceId != null ? provinceId : map[columnProvinceId],
      postalCode: map[columnPostalCode],
      phoneAreaCode: map[columnPhoneAreaCode],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      columnId: id,
      columnLocation: location,
      columnCityId: cityId,
      columnProvinceId: provinceId,
      columnPostalCode: postalCode,
      columnPhoneAreaCode: phoneAreaCode,
    };
  }

  static List<PostalArea> fromList(PostalAreaList list) {
    return list.data.map<PostalArea>((map) => PostalArea.fromMap(map, list.provinceId)).toList();
  }
}

// Database Helper for Model Province
class PostalAreaProvider {
  final Database _database;

  PostalAreaProvider(this._database);

  Future<List<PostalArea>> getAll() async {
    List<Map> list = await _database.rawQuery('SELECT * FROM ${PostalArea.tableName}');

    return list.map<PostalArea>((map) => PostalArea.fromMap(map)).toList();
  }

  Future batchInsert(List<PostalArea> postalAreas) async {
    final batch = _database.batch();
    postalAreas.forEach((area) {
      batch.insert(PostalArea.tableName, area.toMap());
    });

    return await batch.commit();
  }
}