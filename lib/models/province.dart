import 'dart:async';
import 'package:sqflite/sqflite.dart';

class Province {
  static final tableName = 'provinces';
  static final columnId = 'id';
  static final columnAbbr = 'key'; // abbr is more semantic
  static final columnName = 'name';

  final int id;
  final String name;
  final String abbr;

  Province({
    this.id,
    this.name,
    this.abbr,
  });

  factory Province.fromMap(Map<String, dynamic> map) {
    return Province(
      id: map[columnId],
      name: map[columnName],
      abbr: map[columnAbbr],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      columnId: id,
      columnName: name,
      columnAbbr: abbr,
    };
  }

  @override
  String toString() {
    return name;
  }
}

// Database Helper for Model Province
class ProvinceProvider {
  final Database _database;

  ProvinceProvider(this._database);

  Future<List<Province>> getAll() async {
    List<Map> list = await _database.rawQuery('SELECT * FROM ${Province.tableName}');

    return list.map<Province>((map) => Province.fromMap(map)).toList();
  }

  Future<List<Province>> searchByName(String searchInput) async {
    List<Map> list = await _database.rawQuery(
      'SELECT * FROM ${Province.tableName} WHERE ${Province.columnName} LIKE \'%$searchInput%\''
    );
    
    return list.map<Province>((map) => Province.fromMap(map)).toList();
  }

  Future batchInsert(List<Province> provinces) async {
    final batch = _database.batch();
    provinces.forEach((province) {
      batch.insert(Province.tableName, province.toMap());
    });

    return await batch.commit();
  }
}