import 'dart:async';
import 'package:sqflite/sqflite.dart';

class City {
  static final String tableName = 'cities';
  static final String columnId = 'id';
  static final String columnName = 'name';
  static final String columnIsCity = 'city';
  static final String columnProvinceId = 'province_id';

  final int id;
  final String name;
  final bool isCity;
  final provinceId;

  City({
    this.id,
    this.name,
    this.isCity,
    this.provinceId,
  });

  factory City.fromMap(Map<String, dynamic> map) {
    return City(
      id: map[columnId] as int,
      name: map[columnName],
      isCity: map[columnIsCity] == 1 || map[columnIsCity] == true ? true : false,
      provinceId: map[columnProvinceId] as int,
    );
  }

  Map<String, dynamic> toMap([bool asJson = false]) {
    return {
      columnId: id,
      columnName: name,
      columnIsCity: asJson ? isCity : isCity ? 1 : 0,
      columnProvinceId: provinceId,
    };
  }

  @override
  String toString() {
    return 'name ${isCity ? 'City' : ''}';
  }
}

// Database Helper for Model City
class CityProvider {
  final Database _database; // TODO: Provide DatabaseClient instead of Database;

  CityProvider(this._database);

  Future<List<City>> getAll() async {
    List<Map> list = await _database.rawQuery('SELECT * FROM ${City.tableName}');

    return list.map<City>((cityJson) => City.fromMap(cityJson)).toList();
  }

  Future<List<City>> searchByName(String searchInput) async {
    List<Map> list = await _database.rawQuery('SELECT * FROM ${City.tableName} WHERE ${City.columnName} LIKE \'%$searchInput%\'');

    return list.map<City>((cityJson) => City.fromMap(cityJson)).toList();
  }

  Future batchInsert(List<City> cities) async {
    final batch = _database.batch();
    cities.forEach((city) {
      batch.insert(City.tableName, city.toMap());
    });

    return await batch.commit();
  }
}