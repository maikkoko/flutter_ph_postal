import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:synchronized/synchronized.dart';
import 'package:ph_postal_codes/models/models.dart';

const TAG = '[PostalPH]';

class DatabaseClient {
  static final String dbName = 'postal.db';
  static final int dbVersion = 1;

  Database _database;
  final _lock = new Lock();

  Database get db {
    return _database;
  }

  Future<String> _getDbPath() async {
    var databasesPath = await getDatabasesPath();
    return join(databasesPath, dbName);
  }

  Future delete() async {
    final path = await _getDbPath();
    return await deleteDatabase(path);
  }

  Future<Database> open() async {
    final path = await _getDbPath();

    if (_database == null) {
      await _lock.synchronized(() async {
        // Check again once entering the synchronized block
        if (_database == null) {
          _database = await openDatabase(
            path,
            version: dbVersion,
            onOpen: _onOpen,
            onCreate: _onCreate,
          );
        }
      });
    }

    return _database;
  }

  Future close() async {
    return await _database.close();
  }

  _onOpen(Database db) async {
    print('$TAG Connected to database $dbName version ${await db.getVersion()}');
  }

  _onCreate(Database db, int version) async {
    await db.transaction((txn) async {
      await txn.execute(
        'CREATE TABLE ${Province.tableName} (' +
          '${Province.columnId} INTEGER PRIMARY KEY,' +
          '${Province.columnName} TEXT,' +
          '${Province.columnAbbr} TEXT' +
        ');'
      );

      await txn.execute(
        'CREATE TABLE ${City.tableName} (' +
          '${City.columnId} INTEGER PRIMARY KEY,' +
          '${City.columnName} TEXT,' +
          '${City.columnIsCity} INTEGER DEFAULT 0,' +
          '${City.columnProvinceId} INTEGER,' +
          'FOREIGN KEY (${City.columnProvinceId}) REFERENCES ${Province.tableName} (${Province.columnId})'
        ');'
      );

      await txn.execute(
        'CREATE TABLE ${PostalArea.tableName} (' +
          '${PostalArea.columnId} INTEGER PRIMARY KEY AUTOINCREMENT,' +
          '${PostalArea.columnLocation} TEXT,' +
          '${PostalArea.columnPostalCode} TEXT,' +
          '${PostalArea.columnPhoneAreaCode} TEXT,' +
          '${PostalArea.columnCityId} INTEGER,' +
          '${PostalArea.columnProvinceId} INTEGER,' +
          'FOREIGN KEY (${PostalArea.columnCityId}) REFERENCES ${City.tableName} (${City.columnId}),' +
          'FOREIGN KEY (${PostalArea.columnProvinceId}) REFERENCES ${Province.tableName} (${Province.columnId})' +
        ');'
      );
    });
 }
}